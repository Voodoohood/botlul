﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;


namespace SlothHood.TwitchApi
{
    class NonAuthSlothClient
    {
        private string _channel;

        public NonAuthSlothClient(string channel)
        {
            _channel = channel;
        }

        public List<string> GetUsersInChat()
        {
            List<string> res = new List<string>();
            string url = "https://tmi.twitch.tv/group/user/" + _channel.Substring(1) + "/chatters";
            WebClient client = new WebClient();
            byte[] rawData = client.DownloadData(url);
            string stringData = Encoding.UTF8.GetString(rawData);
            stringData = stringData.Replace("\n", "");
            stringData = stringData.Replace(" ", "");
            StringParser parser = new StringParser();
            parser.ParseChatters(stringData);
            res = parser.GetChatters();
            Console.WriteLine(stringData);
            return res;
        }


    }
}
