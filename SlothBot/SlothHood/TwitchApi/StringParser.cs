﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlothHood.TwitchApi
{
    class StringParser
    {
        private List<string> _chatters;
        private List<string> _mods;
        private List<string> _staff;
        private List<string> _admins;
        private List<string> _global_mods;
        private List<string> _viewers;
        private List<string> _links;
        private int _chattercount;

        public void ParseChatters(string rawchatters)
        {
            rawchatters = rawchatters.Replace(" ", "");
            rawchatters = rawchatters.Replace("/n", "");
            int length = rawchatters.Length;
            StringBuilder chattername = new StringBuilder();
            List<string> zw = new List<string>();
            bool wordstart = true;

            for (int i = 0; i < length; i++)
            {
                if (rawchatters[i] == '"' && wordstart)
                {
                    chattername = new StringBuilder();
                    wordstart = false;
                }
                else if (rawchatters[i] == '"' && !wordstart)
                {
                    string name = chattername.ToString();
                    zw.Add(name);
                    chattername = new StringBuilder();
                }
                else
                {
                    chattername.Append(rawchatters[i]);
                }
            }

            int countToDelete = 0;
            List<string> toDelete = new List<string>();
            foreach(string s in zw)
            {
                if (s.Contains('{') || s.Contains(',') || s.Contains('[') || s.Contains(']') || s.Contains('}')||s.Contains(':')){
                    toDelete.Add(s);
                }
                if (s == "_links") toDelete.Add(s);
                if (s == "staff") toDelete.Add(s);
                if (s == "admins") toDelete.Add(s);
                if (s == "global_mods") toDelete.Add(s);
                if (s == "viewers") toDelete.Add(s);
                if (s == "chatters") toDelete.Add(s);
                if (s == "chatter_count") toDelete.Add(s);
                if (s == "moderators") toDelete.Add(s);

            }

            foreach(string s in toDelete)
            {
                zw.Remove(s);
            }

            _chatters = zw;

        }

        public List<string> GetChatters() { return _chatters; }

        public List<string> GetMods() { return _mods; }

        public List<string> GetStaff() { return _staff; }

        public List <string> GetAdmins() { return _admins; }

        public List<string> GetGlobalMods() { return _global_mods; }

        public List<string> GetViewers() { return _viewers; }

        public StringParser()
        {
            _mods = new List<string>();
            _chatters = new List<string>();
            _staff = new List<string>();
            _admins = new List<string>();
            _global_mods = new List<string>();
            _viewers = new List<string>();
            _links = new List<string>();
        }
    }
}
