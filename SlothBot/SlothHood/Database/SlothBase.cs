﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using SlothHood.Logging;


/*
 * 
 *Table username / lastseen / points / mod 
 * 
 * paht : "SlothData.sqlite"
 */

namespace SlothHood.Database
{
    class SlothBase
    {
        private string _path;
        private SQLiteConnection _connection;
        private Object _lock = new object();

        public SlothBase(string path)
        {
            _path = path;
        }

        public void CreateConnection()
        {
            _connection = new SQLiteConnection();
            _connection.ConnectionString = "Data Source=" + _path;
            SlothLogger.log("Database connection established");         
        }

        public void CreateTable()
        {
            if (_connection != null)
            {
                SQLiteCommand command = new SQLiteCommand(_connection);
                command.CommandText = "create table users (name varchar(200) primary key, lastseen varchar(200),points int,mod int);";
                command.ExecuteNonQuery();
            }
            else SlothLogger.log("Database connection not connected");
            
        }

        public void Insert(string username, string stamp)
        {
            lock (_lock)
            {
                if (_connection != null)
                {
                    _connection.Open();
                    SQLiteCommand command = new SQLiteCommand(_connection);
                    command.CommandText = "INSERT INTO users (name, lastseen , points , mod) VALUES('" + username.ToLower() + "' ,'" + stamp + "',0,0)";
                    command.ExecuteNonQuery();
                    _connection.Close();
                    SlothLogger.log("Added the new user " + username + " to the database");
                }
                else
                {
                    SlothLogger.log("Failed to add the new user to the database because the connection wanst established");
                }
            }          
        }

        public string GetLastSeen(string username)
        {
            lock (_lock)
            {
                if (_connection != null)
                {
                    _connection.Open();
                    SQLiteCommand command = new SQLiteCommand(_connection);
                    StringBuilder build = new StringBuilder("SELECT lastseen FROM users ");
                    build.Append("WHERE name='" + username + "'");
                    command.CommandText = build.ToString();
                    SQLiteDataReader reader = command.ExecuteReader();
                    string res = reader["lastseen"] as string;
                    SlothLogger.log("Correctly returned the lastseen stamp");
                    _connection.Close();
                    return res;
                }
                else
                {
                    SlothLogger.log("Failed to correctly return the lastseen stamp because the connection wasnt established");
                    return null;
                }
            }
        }

        public int GetPoints(string username)
        {
            lock (_lock)
            {
                if (_connection != null)
                {
                    _connection.Open();
                    SQLiteCommand command = new SQLiteCommand(_connection);
                    StringBuilder build = new StringBuilder("SELECT points FROM users ");
                    build.Append("WHERE name='" + username + "'");
                    command.CommandText = build.ToString();
                    SQLiteDataReader reader = command.ExecuteReader();
                    string zwres = reader.GetValue(0).ToString();
                    int res = Int32.Parse(zwres);
                    SlothLogger.log("Points correctly returned");
                    _connection.Close();
                    return res;
                }
                else
                {
                    SlothLogger.log("Failed to correctly return the points because the connection wasnt established");
                    return 0;
                }
            }
        }

        public int GetMod(string username)
        {
            lock (_lock)
            {
                if (_connection != null)
                {
                    _connection.Open();
                    SQLiteCommand command = new SQLiteCommand(_connection);
                    StringBuilder build = new StringBuilder("SELECT mod FROM users ");
                    build.Append("WHERE name='" + username + "'");
                    command.CommandText = build.ToString();
                    SQLiteDataReader reader = command.ExecuteReader();
                    string cached = reader.GetValue(0).ToString();
                    int resint = Int32.Parse(cached);
                    SlothLogger.log("Mod status correctly returned");
                    _connection.Close();
                    return resint;
                }
                else
                {
                    SlothLogger.log("Failed to correctly return the mod status because the connection wasnt estblished");
                    return 0;
                }
            }
         
        }

        public void UpdateLastSeen(string username, string val)
        {
            lock (_lock)
            {
                if (_connection != null)
                {
                    _connection.Open();
                    SQLiteCommand command = new SQLiteCommand(_connection);
                    string str = "update users set lastseen ='" + val + "' where name='" + username + "'";
                    StringBuilder build = new StringBuilder("UPDATE users");
                    build.Append("SET lastseen='" + val + "' ");
                    build.Append("WHERE name='" + username + "'");
                    command.CommandText = str;
                    command.ExecuteNonQuery();
                    SlothLogger.log("Updated the lastseen stamp correctly");
                    _connection.Close();
                }
                else
                {
                    SlothLogger.log("Failed to correctly update the lastseen stamp because the connection wasnt established");
                }
            }     
        }

        public void UpdatePoints(string username,int points)
        {
            lock (_lock)
            {
                if (_connection != null)
                {
                    _connection.Open();
                    SQLiteCommand command = new SQLiteCommand(_connection);
                    StringBuilder build = new StringBuilder("UPDATE users ");
                    build.Append("SET points =" + points + " ");
                    build.Append("WHERE name='" + username + "'");
                    command.CommandText = build.ToString();
                    command.ExecuteNonQuery();
                    _connection.Close();
                    SlothLogger.log("Updated the points correctly");
                }
                else
                {
                    SlothLogger.log("Failed to correctly update the points because the connection wasnt established");
                }
            }
            
        }

        public void UpdateMod(string username,int status)
        {
            lock (_lock)
            {
                if (_connection != null)
                {
                    _connection.Open();
                    SQLiteCommand command = new SQLiteCommand(_connection);
                    String str = "update users set mod = " + status + " where name ='" + username + "'";
                    StringBuilder build = new StringBuilder("UPDATE users ");
                    build.Append("SET mod=" + status + " ");
                    build.Append("WHERE username='" + username + "'");
                    command.CommandText = str;
                    command.ExecuteNonQuery();
                    _connection.Close();
                    SlothLogger.log("Updated the mod status correctly");
                }
                else
                {
                    SlothLogger.log("Failed to correctly update the mod status because the connection wasnt established");
                }
            }
         
        }

        public Boolean IsUserRegistered(string user)
        {
            bool res = false;

            lock (_lock)
            {
                if (_connection != null)
                {
                    _connection.Open();
                    SQLiteCommand command = new SQLiteCommand(_connection);
                    StringBuilder build = new StringBuilder("select * from users");
                    command.CommandText = build.ToString();
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        string name = reader.GetString(0);
                        if (name == user)
                        {
                            res = true;
                        }
                    }
                    _connection.Close();
                }
                else
                {
                    SlothLogger.log("Connection not established");
                }
            }
            
            return res;
        }

        //Triva section

        public void CreateTriviaTable()
        {
            if (_connection != null)
            {
                _connection.Open();
                SQLiteCommand command = new SQLiteCommand(_connection);
                command.CommandText = "create table trivia (question varchar(500) primary key, awnser varchar(500) not null);";
                command.ExecuteNonQuery();
                _connection.Close();
            }
            else SlothLogger.log("Database connection not connected");
        }


        public void InsertQnA(string question,string awnser)
        {
            if (_connection != null)
            {
                _connection.Open();
                SQLiteCommand command = new SQLiteCommand(_connection);
                command.CommandText = "insert into trivia ( question, awnser ) values ('" + question.ToLower() + "' ,'" + awnser.ToLower() + "')";
                command.ExecuteNonQuery();
                _connection.Close();
            }
            else SlothLogger.log("Database connection not connected");
        }

        public Dictionary<string,string> GetQnA()
        {
            lock (_lock)
            {
                Dictionary<string, string> questionsAndAwnsers = new Dictionary<string, string>();
                if (_connection != null)
                {
                    _connection.Open();
                    SQLiteCommand command = new SQLiteCommand(_connection);
                    command.CommandText = "select * from trivia";
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        questionsAndAwnsers.Add(reader.GetString(0), reader.GetString(1));
                    }
                    _connection.Close();
                }
                else SlothLogger.log("Database connection not connected");
                return questionsAndAwnsers;
            } 
        }

        public List<string> GetQuestions()
        {
            lock (_lock)
            {
                List<string> questions = new List<string>();
                if (_connection != null)
                {
                    _connection.Open();
                    SQLiteCommand command = new SQLiteCommand(_connection);
                    command.CommandText = "select * from trivia";
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        questions.Add(reader.GetString(0));
                    }
                    _connection.Close();
                }
                else SlothLogger.log("Database connection not connected");

                return questions;
            }           
        }

    }
}
