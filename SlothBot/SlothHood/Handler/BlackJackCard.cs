﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlothHood.Handler
{
    class BlackJackCard
    {
        private string _name;
        private int _points;

        public BlackJackCard(string name,int points)
        {
            _name = name;
            _points = points;
        }

        public int GetPoints() { return _points; }
        public string GetName() { return _name; }
    }
}
