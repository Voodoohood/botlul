﻿using System;
using System.Collections.Generic;
using SlothHood.IrcMessages;
using SlothHood.TCPClient;
using SlothHood.Database;
using SlothHood.Worker;
using System.Threading;
using SlothHood.Logging;

namespace SlothHood.Handler
{
    class TriviaHandler
    {
        private Dictionary<string, string> _QnA;
        private List<string> _questions;
        private SlothBase _database;
        private TriviaWorker _task;
        private bool _workerActive;
        private Thread _worker;
        private string _activeQuestion;
        private string _activeAwnser;
        private int _questionCounter;

        public TriviaHandler(SlothBase database)
        {
            _database = database;
            _workerActive = false;
            _QnA = _database.GetQnA();
            _questions = _database.GetQuestions();
            _questionCounter = 0;
        }

        //Check method to find wich other method is there to call. Gets Called by the IrcMessageHandler Object
        public void TrivaHandel(PrivateMessage msg,string stamp,SlothTCPClient client)
        {
            string real_msg = msg.GetPrvMsg();
            string sender = msg.GetSender().ToLower();
            string[] msgsplit = real_msg.Split(' ');

            if (_database.GetMod(sender) == 1)
            {
                if (msgsplit[0] == "#StartTrivia" && !_workerActive)
                {
                    TriviaStart(client);

                }

                if (msgsplit[0] == "#EndTrivia" && _workerActive)
                {
                    TriviaEnd(client);
                }
                if (msgsplit[0] == "#next" && _workerActive && _questions.Count!=0)
                {
                    NextQuestionModCommand(client);
                }
                else if(_questions.Count == 0 && msgsplit[0]=="#next")
                {

                    _questions = _database.GetQuestions();
                    NextQuestionModCommand(client);
                }

            }
            if (msgsplit[0] == "#question")
            {
                string question = "The active question is: ";
                if(_activeQuestion == null)
                {
                    client.SendChanelMessage(question + " no question LUL");   
                }
                else
                {
                    client.SendChanelMessage(question + _activeQuestion);
                }
            }
            if (_workerActive)
            {
                if (msgsplit[0] == "#skip" && _questions.Count !=0)
                {
                    int points = _database.GetPoints(sender.ToLower());
                    if(points >= 50)
                    {
                        SkipQuestion(client, sender, points);
                    }
                    else
                    {
                        client.SendChanelMessage(sender + " you dont have enough points to skip (50 required)");
                    }

                }
                else if(_questions.Count == 0 && msgsplit[0] == "#skip")
                {
                    _questions = _database.GetQuestions();
                    int points = _database.GetPoints(sender.ToLower());
                    if (points >= 50)
                    {
                        SkipQuestion(client, sender, points);
                    }
                    else
                    {
                        client.SendChanelMessage(sender + " you dont have enough points to skip (50 required)");
                    }
                }
            }
            if(_activeAwnser != null)
            {
                if (real_msg.ToLower().Contains(_activeAwnser) && _questions.Count!=0)
                {
                    AnswerFound(client, sender);
                }
                else if(real_msg.ToLower().Contains(_activeAwnser) && _questions.Count == 0)
                {
                    _questions = _database.GetQuestions();
                    AnswerFound(client, sender);
                }
            }

        }

        private void NextQuestionModCommand(SlothTCPClient client)
        {
            string answer = _activeAwnser;
            Random rnd = new Random();
            int next = rnd.Next(0, _questions.Count);
            _questions.Remove(_activeQuestion);
            _activeQuestion = _questions[next];

            string newawnser;
            if (_QnA.TryGetValue(_activeQuestion, out newawnser))
            {
                _activeAwnser = newawnser;
            }
            else SlothLogger.log("Failed to load the answer to question: " + _activeQuestion);
            client.SendChanelMessage("The awnser to the old question was: " + answer + " . The new question is: " + _activeQuestion);
        }

        private void AnswerFound(SlothTCPClient client, string sender)
        {
            Random rnd = new Random();
            int num = rnd.Next(0, _questions.Count);
            _questions.Remove(_activeQuestion);
            _activeQuestion = _questions[num];
            string answer;
            if (_QnA.TryGetValue(_activeQuestion, out answer))
            {
                _activeAwnser = answer;
            }
            else SlothLogger.log("Failed to load the answer to question: " + _activeQuestion);

            int points = _database.GetPoints(sender.ToLower());
            _database.UpdatePoints(sender.ToLower(), points + 100);
            client.SendChanelMessage("Congrats " + sender + " you found the answer and won 100 points");
            client.SendChanelMessage("The next question is: " + _activeQuestion);
        }
        
        private void SkipQuestion(SlothTCPClient client, string sender, int points)
        {
            Random rnd = new Random();
            int num = rnd.Next(0, _questions.Count);
            _questions.Remove(_activeQuestion);
            _activeQuestion = _questions[num];
            string answer;
            if (_QnA.TryGetValue(_activeQuestion, out answer))
            {
                _activeAwnser = answer;
            }
            else SlothLogger.log("Failed to load the answer to question: " + _activeQuestion);
            _database.UpdatePoints(sender, points - 50);
            client.SendChanelMessage(sender + " you skipped the active question for 50 points");
            client.SendChanelMessage("The new question is: " + _activeQuestion);
        }

        //Ending the trivia game
        private void TriviaEnd(SlothTCPClient client)
        {
            _task.ShutDown();
            _workerActive = false;
            _activeAwnser = null;
            _activeQuestion = null;
            _questionCounter = 0;
            client.SendChanelMessage("Trivia ended FeelsBadMan");
        }

        //Starting the trivia game
        private void TriviaStart(SlothTCPClient client)
        {
            Random rnd = new Random();
            int next = rnd.Next(0, _questions.Count);
            _activeQuestion = _questions[next];
            string awnser;
            if (_QnA.TryGetValue(_activeQuestion, out awnser))
            {
                _activeAwnser = awnser;
            }
            else SlothLogger.log("Failed to load the answer to the question" + _activeQuestion);
            _workerActive = true;
            client.SendChanelMessage("Trivia started! FeelsGoodMan");
            client.SendChanelMessage("The question at the moment is: " + _activeQuestion);
        }
    }
}
