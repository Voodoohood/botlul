﻿using System;

using SlothHood.TCPClient;
using System.Net;
using System.Net.Sockets;
using SlothHood.Logging;
using SlothHood.IrcMessages;
using SlothHood.Database;

namespace SlothHood.Handler
{
    class IrcMessageHandler
    {

        private SlothBase _database;
        private TriviaHandler _triviaHandler;
        private BlackJackHandler _blackJackHandler;

        public IrcMessageHandler(SlothBase database)
        {
            _triviaHandler = new TriviaHandler(database);
            _blackJackHandler = new BlackJackHandler(database);
            _database = database;
        }

        public void Handle(IrcMessage message,SlothTCPClient client)
        {

            SlothLogger.log("Handler started");
            DateTime stamp = DateTime.Now;
            string formatedstamp = stamp.ToString("dd.MM.yyyy HH:mm:ss");
            
            //Ping message handling
            if (message is PingMessage)
            {
                PingMessage msg = message as PingMessage;
                if(msg != null)
                {
                    string info = msg.ReturnServerInfo();
                    client.SendRawLine("PONG SlothBot "+info);
                    SlothLogger.log("Handled a Pong message");
                }
            }

            //Private message handling
            if(message is PrivateMessage)
            {

                PrivateMessage msg = message as PrivateMessage;
                HandlePrivateMessage(msg, formatedstamp, client);
                HandleTriviaPrivatMessage(msg, formatedstamp, client);
                HandleBlackJackMessage(msg, client);
                SlothLogger.log("Handled a PrivateMessage");
            }

            //Join message handling
            if(message is JoinMessage)
            {
                JoinMessage msg = message as JoinMessage;
                if(msg != null && _database != null)
                {
                    string sender = msg.GetSender();
                    if (!_database.IsUserRegistered(sender))
                    {
                        _database.Insert(sender, formatedstamp);
                    }
                    else
                    {
                        _database.UpdateLastSeen(sender, formatedstamp);
                    }
                    SlothLogger.log("User " + sender + " joined the channel");
                }
            }

            
        }

        private void HandlePrivateMessage(PrivateMessage msg,string stamp,SlothTCPClient client)
        {
            var splitted = msg.GetPrvMsg().Split(' ');
            string sender = msg.GetSender();

            if (_database.IsUserRegistered(sender))
            {
                _database.UpdateLastSeen(sender, stamp);
            }

            if (splitted[0] == "#points")
            {
                if (_database.IsUserRegistered(sender))
                {
                    int points = _database.GetPoints(sender);
                    client.SendChanelMessage(sender + " you  have " + points + " points");
                }
            }
            else if(splitted[0] == "#gamble")
            {
                if (splitted.Length >= 2)
                {
                    string numberstring = splitted[1];
                    int number;
                    bool isNumeric = int.TryParse(numberstring, out number);

                    string user = msg.GetSender();
                    int userpoints = _database.GetPoints(user.ToLower());
                    if (isNumeric)
                    {
                        if (number < userpoints)
                        {
                            Random rnd = new Random();
                            int roll = rnd.Next(0, 1);
                            if (roll == 0)
                            {
                                int fin = userpoints - number;
                                _database.UpdatePoints(user.ToLower(), userpoints - number);
                                client.SendChanelMessage(sender + " you lost FeelsBadMan and now have "+fin+" points");
                            }
                            else
                            {
                                int fin = userpoints + number;
                                _database.UpdatePoints(user.ToLower(), userpoints + number);
                                client.SendChanelMessage(sender + " you won ! FeelsGoodman and now have "+fin+" points");
                            }
                        }
                    }

                }
            }
            else if(splitted[0] == "#lastseen")
            {
                if (splitted.Length >= 2)
                {
                    string user = splitted[1].ToLower();
                    Console.WriteLine(user);
                    if (_database.IsUserRegistered(user))
                    {
                        string userstamp = _database.GetLastSeen(user.ToLower());
                        client.SendChanelMessage("The user " + user + " was last seen " + userstamp);
                    }
                }
            }

        }

        private void HandleTriviaPrivatMessage(PrivateMessage msg,string stamp,SlothTCPClient client)
        {
            _triviaHandler.TrivaHandel(msg, stamp, client);
        }

        private void HandleBlackJackMessage(PrivateMessage msg,SlothTCPClient client)
        {
            _blackJackHandler.HandleBlackJackCommand(msg, client);
        }
    }
}
