﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlothHood.Database;
using SlothHood.Logging;
using SlothHood.TCPClient;
using SlothHood.IrcMessages;


namespace SlothHood.Handler
{
    class BlackJackHandler
    {
        private bool _active;
        private SlothBase _database;
        private BlackJackGame _game;
        private bool _gameactive;
        

        public BlackJackHandler(SlothBase database)
        {
            _database = database;
            _active = false;
            _gameactive = false;
        }

        public void ActivateBlackJack()
        {
            _active = true;
        }

        public void HandleBlackJackCommand(PrivateMessage msg,SlothTCPClient client)
        {
            string sender = msg.GetSender();
            string real_msg = msg.GetPrvMsg();
            string[] split_msg = real_msg.Split(' ');

            if (_database.GetMod(sender) > 0)
            {
                if (split_msg[0] == "#StartBlackJackGame")
                {
                    _game = new BlackJackGame(_database, client,this);
                    _gameactive = true;
                    _game.BuyInPhase();
                }
            }
            if (_gameactive)
            {
                _game.UserPhase(sender, split_msg[0]);
                if (split_msg.Length > 1)
                {
                    if (split_msg[0] == "#Enter")
                    {
                        int num;
                        bool isNumeric = int.TryParse(split_msg[1], out num);
                        if (isNumeric)
                        {
                            int userpoints = _database.GetPoints(sender);
                            if (userpoints > num)
                            {
                                _game.AddUser(sender, num);
                                _database.UpdatePoints(sender, userpoints - num);
                                
                            }
                        }
                    }
                }
                
            }



        }

        public void EndGame()
        {
            _game = null;
            _gameactive = false;
        }
    }
}
