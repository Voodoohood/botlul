﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlothHood.TCPClient;
using SlothHood.Logging;
using SlothHood.Database;
using System.Timers;
using System.Threading;

namespace SlothHood.Handler
{
    class BlackJackGame
    {
        private List<string> _bustedUsers;
        private int _dealerPoints;
        private List<string> _users;
        private Dictionary<string, int> _bets;
        private Dictionary<string, int> _points;
        private SlothTCPClient _client;
        private SlothBase _database;
        private bool _BuyInPossible;
        private BlackJackDeck _deck;
        private string _activeuser;
        private bool _userPhaseRunning; 
        private int _userCounter;
        private BlackJackHandler _handler;

        public BlackJackGame (SlothBase database,SlothTCPClient client,BlackJackHandler handler)
        {
            _handler = handler;
            _bustedUsers = new List<string>();
            _userCounter = 0;
            _dealerPoints = 0;
            _bets = new Dictionary<string, int>();
            _points = new Dictionary<string, int>();
            _client = client;
            _database = database;
            _users = new List<string>();
            _BuyInPossible = true;
            _deck = new BlackJackDeck();
            _deck.Shuffle();
        }

        public void AddUser(string username,int bet)
        {
            if (_users.Count < 8 && _BuyInPossible)
            {
                _users.Add(username);
                _bets.Add(username, bet);
                _points.Add(username, 0);
                _client.SendChanelMessage(username + " you were added to the table with a bet of " + bet);
            }
            else
            {
                _client.SendChanelMessage("Sorry " + username + " you are either to late to the party or there are already too much players! FeelsBadMan");
            }
        }


        public void BuyInPhase()
        {
            _client.SendChanelMessage("A new black jack game started enter #Enter <bet> to take part, you have 1 min time");
            System.Threading.Timer buyintimer = null;
            buyintimer = new System.Threading.Timer((obj) =>
            {
                BuyInPhaseEnd();
                buyintimer.Dispose();
            },null,60000,System.Threading.Timeout.Infinite);
            
        }

        public void BuyInPhaseEnd()
        {
            _BuyInPossible = false;
            StringBuilder build = new StringBuilder();
            foreach (string user in _users)
            {
                build.Append(user + ", ");
            }
            _client.SendChanelMessage("The buy in phase stopped! In the game are the players: " + build.ToString());
            FirstCardRound();
        }

        public void FirstCardRound()
        {
            BlackJackCard dealerCard = _deck.GetTop();
            int dCardVal = dealerCard.GetPoints();
            string dCardName = dealerCard.GetName();
            _dealerPoints = dCardVal;
            _client.SendChanelMessage("The dealer drew a " + dCardName + " and has " + dCardVal + " points now");
            StringBuilder build = new StringBuilder();
            foreach (string user in _users)
            {
                BlackJackCard card = _deck.GetTop();
                int cardVal = card.GetPoints();
                string cardName = card.GetName();
                _points[user] = cardVal;
                _client.SendChanelMessage(user + " drew a " + cardName + " and has " + cardVal + " points");
            }
            SecondCardRound();
           
        }

        public void SecondCardRound()
        {
            if (_users.Count > 0)
            {
                _client.SendChanelMessage("Second card round for the players!");
                foreach (string user in _users)
                {
                    BlackJackCard card = _deck.GetTop();
                    int cardVal = card.GetPoints();
                    string cardName = card.GetName();
                    _points[user] = _points[user] + cardVal;
                    _client.SendChanelMessage(user + " drew a " + cardName + " and has " + _points[user] + " points");
                }
                _activeuser = _users[0];
                _userPhaseRunning = true;
                _client.SendChanelMessage("The second card round stopped and the user phase started your options are #hit | #stay starting with user " + _activeuser);
                NextPlayerTimer();
            }
            else
            {
                _client.SendChanelMessage("No players so shutting down!");
            }
           
           
        }

        private void NextPlayerTimer()
        {
            System.Threading.Timer usertimer = null;
            usertimer = new System.Threading.Timer((obj) =>
            {
                NextPlayer();
                usertimer.Dispose();
            }, null, 30000, System.Threading.Timeout.Infinite);
        }

        private void NextPlayer()
        {
            if(_userCounter < ((_users.Count) - 1))
            {
                string user = _activeuser;
                _userCounter++;
                _activeuser = _users[_userCounter];
                _client.SendChanelMessage("The player " + _userCounter + " timed out the next player is "+_activeuser);
                NextPlayerTimer();
            }
        }

        public void UserPhase(string username,string command)
        {
            if (_userPhaseRunning)
            {
                if(username == _activeuser)
                {
                    if(command == "#hit")
                    {

                        BlackJackCard card = _deck.GetTop();
                        int cardVal = card.GetPoints();
                        string cardName = card.GetName();
                        _points[username] = _points[username] + cardVal;
                        if (_points[username] > 21)
                        {
                            _client.SendChanelMessage(username + " you drew a " + cardName + " and got busted with " + _points[username]);
                            _bustedUsers.Add(username);
                            _userCounter++;
                            if (_userCounter < _users.Count)
                            {
                                _activeuser = _users[_userCounter];
                                _client.SendChanelMessage("the next player is " + _activeuser + " you have now the options #hit | #stay");
                                NextPlayerTimer();
                            }
                            else
                            {
                                _userPhaseRunning = false;
                                EndPhase();
                            }
                           
                        }
                        else
                        {
                            _client.SendChanelMessage(username + " you drew a " + cardName + " and now have " + _points[username]);
                            NextPlayerTimer();
                        }
                    }
                    else if(command == "#stay")
                    {
                        _userCounter++;
                        if (_userCounter < _users.Count)
                        {
                            _activeuser = _users[_userCounter];
                            _client.SendChanelMessage(username + " you sopped at " + _points[username] + " the next player is " + _activeuser + " you have now the optins #hit | #stay");
                            NextPlayerTimer();
                        }
                        else
                        {
                            _userPhaseRunning = false;
                            EndPhase();
                        }
                       
                    }
                }
            }
        }

        public void EndPhase()
        {
            _client.SendChanelMessage("The end phase started!");
            StringBuilder dealerbuild = new StringBuilder("The dealer drew the cards until he had more than or 17 points the cards: ");
            while (_dealerPoints < 17)
            {
                BlackJackCard card = _deck.GetTop();
                int cardVal = card.GetPoints();
                string cardNam = card.GetName();
                _dealerPoints = _dealerPoints + cardVal;
                dealerbuild.Append(cardNam + " " + cardVal + " |");
            }
            dealerbuild.Append(" Final dealer points:" + _dealerPoints);
            _client.SendChanelMessage(dealerbuild.ToString());
            List<string> possibleWinners = new List<string>();

            foreach (string user in _users)
            {
                if (!_bustedUsers.Contains(user))
                {
                    possibleWinners.Add(user);
                }
            }
            bool dealerbust = false;
            foreach(string user in possibleWinners)
            {
                if(_dealerPoints > 21)
                {
                    int userpoints = _database.GetPoints(user);
                    int bet = _bets[user];
                    _database.UpdatePoints(user, userpoints + 2 * bet);
                    dealerbust = true;
                }else
                {
                    int points = _points[user];
                    if (points > _dealerPoints)
                    {
                        int userpoints = _database.GetPoints(user);
                        int bet = _bets[user];
                        _database.UpdatePoints(user, userpoints + 2 * bet);
                        _client.SendChanelMessage("congrats " + user + " you won your bet back 2 times! FeelsGoodMan");
                    }
                    else if (points == _dealerPoints)
                    {
                        int userpoints = _database.GetPoints(user);
                        int bet = _bets[user];
                        _client.SendChanelMessage("congrats " + user + " you have the same points as the dealer so you get your bet back! SeemsGood");
                    }
                }
               
            }
            if (dealerbust)
            {
                _client.SendChanelMessage("Every player who didnt busted himself won because the dealer busted ,congrats! PogChamp");
            }
            _client.SendChanelMessage("The Black Jack game ended!");
            _handler.EndGame();
        }


    }
}
