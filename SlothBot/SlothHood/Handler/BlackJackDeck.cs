﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlothHood.Handler
{
    class BlackJackDeck
    {
        private List<BlackJackCard> _cards;
        private bool _created;
        
        public BlackJackDeck()
        {
            _created = false;
            _cards = new List<BlackJackCard>();
            CreateDeck();
        }

        public void Shuffle()
        {
            if (_created && _cards.Count>0)
            {
                for (int j = 0; j < 10000; j++)
                {
                    for (int i = 0; i < _cards.Count; i++)
                    {
                        Random ran = new Random();
                        int partner = ran.Next(0, _cards.Count - 1);
                        Swap(i, partner);
                    }
                }
            }
        }

        public BlackJackCard GetTop()
        {
            BlackJackCard topcard = null;

            if (_created)
            {
                topcard = _cards[0];
                _cards.Remove(topcard);
                return topcard;
            }
            return topcard;
        }

        public void CreateDeck()
        {
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("two",2);
                _cards.Add(card);
            }
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("three", 3);
                _cards.Add(card);
            }
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("four", 4);
                _cards.Add(card);
            }
            for(int i= 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("five", 5);
                _cards.Add(card);
            }
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("six", 6);
                _cards.Add(card);
            }
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("seven", 7);
                _cards.Add(card);
            }
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("eight", 8);
                _cards.Add(card);
            }
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("nine", 9);
                _cards.Add(card);
            }
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("ten", 10);
                _cards.Add(card);
            }
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("jack", 10);
                _cards.Add(card);
            }
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("queen", 10);
                _cards.Add(card);
            }
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("king", 10);
                _cards.Add(card);
            }
            for(int i = 0; i < 24; i++)
            {
                BlackJackCard card = new BlackJackCard("ace", 11);
            }
            _created = true;
        }

        public bool IsEmpty()
        {
            bool res = _cards.Count == 0;
            return res;
        }

        private void Swap(int place0,int place1)
        {
            BlackJackCard card0 = _cards[place0];
            _cards[place0] = _cards[place1];
            _cards[place1] = card0;
        }
    }
}
