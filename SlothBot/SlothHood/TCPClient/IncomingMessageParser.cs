﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlothHood.IrcMessages;
using SlothHood.Logging;

namespace SlothHood.TCPClient
{
    public static class IncomingMessageParser
    {
        public static IrcMessage ParseIncomingMessage(string msg)
        {
            IrcMessage res = null;
            DateTime stamp = DateTime.Now;
            string formatedStamp = stamp.ToString("dd.MM.yyyy HH:mm:ss");

            if(msg.StartsWith("PING"))
            {
                var split = msg.Split(new[] { ' ' }, 2);
                if(split.Length == 2)
                {
                    
                    string info = split[1];
                    res = new PingMessage(msg, formatedStamp, info);
                    SlothLogger.log("Created a PingMessage");
                }  
            }
            else
            {
                var split = msg.Split(new[] { ' ' }, 4);

                if (split.Length >= 3)
                {
                    if (split[1].Equals("PRIVMSG"))
                    {
                        string realmsg = split[3].Substring(1);
                        string chanel = split[2].Substring(1);
                        var secondsplit = split[0].Split('!');
                        string sender = secondsplit[0].Substring(1);
                        
                        res = new PrivateMessage(msg, formatedStamp, realmsg, sender);
                        SlothLogger.log("Created a PrivateMessage");
                    }
                    if (split[2].Equals("JOIN"))
                    {
                        string sender = split[1].Substring(1);
                        res = new JoinMessage(msg, formatedStamp, sender);
                        SlothLogger.log("Created a JoinMessage");
                    }                    
                }
            }

            return res;
        }
    }
}
