﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using SlothHood.Logging;
using System.IO;
using System.Threading;
using System.Collections;
using SlothHood.TwitchApi;
using SlothHood.Worker;
using SlothHood.Database;

namespace SlothHood.TCPClient
{
     class SlothTCPClient
    {
        private NonAuthSlothClient _twitchapi;
        private TcpClient _socket;
        private NetworkStream _stream;
        private string _hostName;
        private string _chanel;
        private int _outDelay;
        private Queue<string> _outQ;
        private StreamReader _reader;
        private PointsWorker _PTask;
        private ReadWorker _RTask;
        private WriteWorker _WTask;
        private UsersCheckWorker _UTask;
        private RuntimeCommandsWorker _RCTask;
        private Thread _Pworker;
        private Thread _Wworker;
        private Thread _Rworker;
        private Thread _UserCheckWorker;
        private Thread _RunComWorker;
        private SlothBase _db;
        

        public SlothTCPClient()
        {
            _socket = new TcpClient();
            _outQ = new Queue<string>();
            _db = new SlothBase("SlothData.sqlite");
            _db.CreateConnection();
        }

        public void Connect(string hostname,int port,int delay)
        {
            this._hostName = hostname;
            this._outDelay = delay;
            IPHostEntry hostEntry = Dns.GetHostEntry(hostname);
            if (hostEntry.AddressList.Length > 0 && !_socket.Connected)
            {
                var ip = hostEntry.AddressList[0];
                _socket.BeginConnect(ip, port, ConnectCallback, null);
            }
            else SlothLogger.log("Failed to convert host name to ip or socket already connected");
        }

        private void ConnectCallback(IAsyncResult res)
        {
            _socket.EndConnect(res);
            SlothLogger.log("Connected to the server "+_hostName);          
            _stream = _socket.GetStream();
            _reader = new StreamReader(_stream);
            _WTask = new WriteWorker(this, _outDelay);
            _RTask = new ReadWorker(_reader,this,_db);
            _RCTask = new RuntimeCommandsWorker(this);
            _Wworker = new Thread(new ThreadStart(_WTask.WriteTask));
            _Rworker = new Thread(new ThreadStart(_RTask.ReadTask));
            _RunComWorker = new Thread(new ThreadStart(_RCTask.RuntimeTask));
            _Rworker.Start();
            _Wworker.Start();
        }

        public void SendRawLine(string line)
        {
            if (_stream != null)
            {
                byte[] package = Encoding.ASCII.GetBytes(line + "\r\n");
                _stream.Write(package, 0, package.Length);
                SlothLogger.log("Sent line " + line);

            }
            else SlothLogger.log("NetWorkStream isnt initalized yet");
        }

        public void ConnectToChannel(string channelname)
        {
            if (_stream != null)
            {
                _chanel = channelname;
                string channelmsg = "JOIN " + _chanel;
                SendRawLine(channelmsg);
                _UTask = new UsersCheckWorker(_db, _chanel);
                _PTask = new PointsWorker(_db, _chanel);
                _Pworker = new Thread(new ThreadStart(_PTask.PointsTask));
                _UserCheckWorker = new Thread(new ThreadStart(_UTask.UserCheckTask));
                _UserCheckWorker.Start();
                _Pworker.Start();
                _RunComWorker.Start();

            }
            else SlothLogger.log("NetWorkStream isnt initalized yet");
        }

        public void SendChanelMessage(string msg)
        {
            if (_chanel != null)
            {
                string sendmsg = "PRIVMSG " + _chanel + " :" + msg;
                _outQ.Enqueue(sendmsg);
            }
            else SlothLogger.log("No chanel joined");
        }

        public Boolean SocketConnected()
        {
            bool connected = _socket.Connected;
            return connected;
        }

        //TODO
        public void NickMessage(string nick)
        {
            if(_outQ != null)
            {
                string msg = "NICK " + nick;
                SendRawLine(msg);
            } 
        }

        public void UserMessage(string nick)
        {
            if(_outQ != null)
            {
                string msg = "USER " + nick + " SlothCo :Concern Doge";
                SendRawLine(msg);
            }

        }

        public void ListMessage()
        {
            if (_socket.Connected)
            {
                string msg = "LIST";
                SendRawLine(msg);
            }
        }

        public void OperMessage()
        {

        }

        public void SpamProtect(int milis)
        {
            _outDelay = milis;
        }

        public void AddMsgToQ(string msg)
        {
            _outQ.Enqueue(msg);
        }

        public string GetMsgFromQ()
        {
            string msg = _outQ.Peek();
            _outQ.Dequeue();
            return msg;
        }

        public bool WriteQEmpty()
        {
            bool res = true;
            if (_outQ.Count > 0)
            {
                res = false;
            }
            return res;
        }
    }
}
