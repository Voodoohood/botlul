﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlothHood.Logging
{
     static class SlothLogger
    {
        public static void log(String msg)
        {
            DateTime stamp = DateTime.Now;
            string formatedStamp = stamp.ToString("dd.MM.yyyy HH:mm:ss");
            Console.WriteLine("{0} : {1}",formatedStamp,msg);
        }
    }
}
