﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlothHood.IrcMessages
{
    public class IrcMessage
    {
        protected string _rawMessage { get; set; }
        protected string _timeStamp { get; set; }
        protected int _messageLength { get; set; }

        public string GetRawMessage() { return _rawMessage; }
        public string GetTimeStamp() { return _timeStamp; }

    }
}
