﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlothHood.IrcMessages
{
    class JoinMessage : IrcMessage
    {
        private string _sender { get; set; }

        public JoinMessage(string rawmessage, string timestamp,string sender)
        {
            base._rawMessage = rawmessage;
            base._timeStamp = timestamp;
            _sender = sender;
        }

        public string GetSender() { return _sender; }

    }
}
