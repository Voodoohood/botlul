﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlothHood.IrcMessages
{
    class PrivateMessage : IrcMessage
    {
        private string _prvmsg { get; set; }
        private string _sender { get; set; }

        public PrivateMessage(string rawmessage,string timestamp,string prvmsg,string sender)
        {
            base._rawMessage = rawmessage;
            base._timeStamp = timestamp;
            _prvmsg = prvmsg;
            _sender = sender; 
        }

        public string GetPrvMsg() { return _prvmsg; }
        public string GetSender() { return _sender; }

    }
}
