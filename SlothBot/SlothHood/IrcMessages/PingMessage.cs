﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SlothHood.IrcMessages
{
    class PingMessage : IrcMessage
    {
        private string _serverInfo { get; set; }

        public PingMessage(string rawmessage, string timestamp, string serverinfo)
        {
            base._rawMessage = rawmessage;
            base._timeStamp = timestamp;
            _serverInfo = serverinfo;
        }

        public string ReturnServerInfo() { return _serverInfo; }

    }
}
