﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlothHood.TCPClient;
using SlothHood.Logging;
using System.Threading;

namespace SlothHood.Worker
{
    class TriviaWorker
    {

        private SlothTCPClient _client;
        private bool _running;
        private string _activeQuestion;

        public TriviaWorker(SlothTCPClient client)
        {
            _activeQuestion = "no question LUL ";
            _running = true;
            _client = client;
        }

        public void ShutDown()
        {
            _running = false;
        }

        public void ChangeActiveQuestion(string question)
        {
            _activeQuestion = question;
        }

        public void TriviaTask()
        {
            SlothLogger.log("TriviaWorker started");
            while (_running)
            {
                _client.SendChanelMessage("The question at the moment is: " + _activeQuestion);
                Thread.Sleep(120000);
            }
            SlothLogger.log("TriviaWorker ended");
        }


    }
}
