﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using SlothHood.Logging;
using SlothHood.TCPClient;

namespace SlothHood.Worker
{
    class WriteWorker
    {
        private SlothTCPClient _client;
        private int _delay;

        public WriteWorker(SlothTCPClient client,int delay)
        {
            _client = client;
            _delay = delay;
        }

        public void WriteTask()
        {
            while (true)
            {
                if (!_client.WriteQEmpty())
                {
                    string msg = _client.GetMsgFromQ();
                    _client.SendRawLine(msg);
                    Thread.Sleep(_delay);
                }
            }
        }
    }
}
