﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlothHood.Database;
using SlothHood.TwitchApi;
using System.Threading;
using SlothHood.Logging;

namespace SlothHood.Worker
{
    class PointsWorker
    {
        private NonAuthSlothClient _client;
        private SlothBase _database;

        public PointsWorker(SlothBase database,string channel)
        {
            _database = database;
            _client = new NonAuthSlothClient(channel); 
        }

        public void PointsTask()
        {
            while (true)
            {
                SlothLogger.log("Started Updating the points of the users");
                List<string> users = _client.GetUsersInChat();
                foreach(string user in users)
                {
                    if (_database.IsUserRegistered(user))
                    {
                        int userpoints = _database.GetPoints(user.ToLower());
                        _database.UpdatePoints(user.ToLower(), userpoints + 5);
                    }
                }
                Thread.Sleep(180000);
            }
        }
    }
}
