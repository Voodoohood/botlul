﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlothHood.TwitchApi;
using SlothHood.Database;
using System.Threading;
using SlothHood.Logging;

namespace SlothHood.Worker
{
    class UsersCheckWorker
    {
        private NonAuthSlothClient _client;
        private SlothBase _database;
        private string _channelname;

        public UsersCheckWorker(SlothBase db,string channelname)
        {
            _database = db;
            _channelname = channelname;
            _client = new NonAuthSlothClient(_channelname);
        }
        
        public void UserCheckTask()
        {
            SlothLogger.log("Started the UserCheckTask ");
            while (true)
            {
                List<string> chatters = _client.GetUsersInChat();
                foreach(string user in chatters)
                {
                    SlothLogger.log("Started checking the chatters");
                    if(_database != null)
                    {
                        DateTime stamp = DateTime.Now;
                        string formated = stamp.ToString("dd.MM.yyyy HH:mm:ss");

                        if (_database.IsUserRegistered(user))
                        {
                            _database.UpdateLastSeen(user,formated);    
                        }
                        else
                        {
                            _database.Insert(user, formated);
                        }
                    }                   
                }
                Thread.Sleep(60000);
                
            }
        }
    }
}
