﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlothHood.TCPClient;

namespace SlothHood.Worker
{
    class RuntimeCommandsWorker
    {
        private SlothTCPClient _client;

        public RuntimeCommandsWorker(SlothTCPClient client)
        {
            _client = client;
        }

        public void RuntimeTask()
        {
            while (true)
            {
                string command = Console.ReadLine();
                if (command.StartsWith("#write"))
                {
                    var split = command.Split(new[] { ' ' }, 2);
                    _client.SendChanelMessage(split[1]);
                }
            }
        }
    }
}
