﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using SlothHood.IrcMessages;
using SlothHood.Handler;
using SlothHood.Logging;
using SlothHood.TCPClient;
using SlothHood.Database;

namespace SlothHood.Worker
{
    class ReadWorker
    {
        private StreamReader _reader;
        private SlothTCPClient _client;
        private IrcMessageHandler _handler;
        private SlothBase _db;

        public ReadWorker(StreamReader reader,SlothTCPClient client,SlothBase db)
        {
            _reader = reader;
            _client = client;
            _db = db;
            SlothLogger.log("Read Worker Object created");
            _handler = new IrcMessageHandler(db);
        }

        public void ReadTask()
        {
            while (true)
            {
                SlothLogger.log("Read Thread waiting for message in the sream");
                string line = _reader.ReadLine();
                Console.WriteLine(line);
                IrcMessage msg = IncomingMessageParser.ParseIncomingMessage(line);
                _handler.Handle(msg, _client);
                SlothLogger.log(line);
            }
        }
    }
}
