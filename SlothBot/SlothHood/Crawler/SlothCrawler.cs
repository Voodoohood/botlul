﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SlothHood.Logging;
using SlothHood.TCPClient;

namespace SlothHood.Crawler
{
     class SlothCrawler
    {

        private List<string> _channelList;
        private List<string> _usersInChannel;
        private SlothTCPClient _client;
        private string _username;

        public SlothCrawler(SlothTCPClient client,string username)
        {
            _client = client;
            _username = username;
            SlothLogger.log("SlothCrawler craeted");
        }

        private bool SearchInChanel()
        {
            bool res = false;

            foreach(string user in _usersInChannel)
            {
                if (user.Equals(_username))
                {
                    res = true;
                    break;
                } 
            }

            return res;
        }

        public void FindUser()
        {
            string res = "Couldnt find user";

            if (_username!=null && _channelList != null)
            {
                foreach (string chanel in _channelList)
                {
                    GetUsersInChannel(chanel);
                    if (SearchInChanel())
                    {
                        res = "Found user in channel: " + chanel;
                        break;
                    }
                }
            }
            _client.SendChanelMessage(res);
            
        }

        public void GetUsersInChannel(string channelname)
        {
            //TODO
        }
    }
}
